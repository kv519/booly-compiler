from lexer.lexer import Lexer

code = """begin
	forsy (inty x = 3; x = 4.5;  =  + )
		if ()
			inny ;
		else 
			outty ;
		endif
		whiley()
			outty ;
		endwhiley
	endforsy
end"""

lexer = Lexer(code)

tokens = []

while True:
    tokens.append(lexer.get_next_token())
    if tokens[-1].lexeme == "\0": break

for token in tokens:
    print("kind: ", token.kind.name, "\tlexeme: ", token.lexeme, "\tvalue: ", token.value)

print(lexer.line_number)
